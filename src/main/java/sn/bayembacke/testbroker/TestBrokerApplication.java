package sn.bayembacke.testbroker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestBrokerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestBrokerApplication.class, args);
    }

}
